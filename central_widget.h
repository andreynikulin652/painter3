#ifndef CENTRAL_WIDGET_H
#define CENTRAL_WIDGET_H

#include <QTimer>
#include <QTimeLine>
#include <QTimerEvent>
#include <QWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QScrollBar>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSlider>
#include <QLineEdit>
#include <QLabel>
#include <vector>
#include <QtMath>
#include "graphic_widget.h"
using namespace std;

class central_widget: public QWidget
{
    Q_OBJECT
private:
    QSlider scbar;
    QTimer timer;
    QGridLayout layout;

    QLineEdit COPoints;
    QLabel lbl_COPoints;

    QLineEdit SOPoints;
    QLabel lbl_SOPoints;

    QLineEdit COFrames;
    QLabel lbl_COFrames;


    graphic_widget glwid;
    QPushButton Animation;

    QLineEdit stepX;
    QLabel label_stepX;

    QLineEdit stepY;
    QLabel label_stepY;

    QRadioButton button_grid;

    int MainX;
    int MainY;


public:
    central_widget();
    void getMainWindowX(int);
    void getMainWindowY(int);

public slots:
    void getX(int);
    void getY(int);

signals:
    void central_sendX(int);
    void central_sendY(int);

};

#endif // CENTRAL_WIDGET_H
