#ifndef GRAPHIC_WIDGET_H
#define GRAPHIC_WIDGET_H

#include <QObject>
#include <QtOpenGL>
#include <QGLWidget>
#include <QMatrix4x4>
#include <QGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions>
#include <QtMath>

class graphic_widget:public QGLWidget
{
    Q_OBJECT
private:
    float* CoordsOfPoints;
    float** ForAnimation;
    float** velocity;
    float** New_LSF;

    void buildVelocity();
    void createNew_LSF();

    //Array of lines of grid by x
    float* grid_lines_x;
    //Array of lines of grid by y
    float* grid_lines_y;
    bool boolGrid;

    int CountOfPoints;
    float SizeOfPoint;

    int VertexLocation;
    int SOPLocation;
    int ScaleMovingLocation;
    int vorlLocation;
    int lineLocation;

    //for engine
    float* forEngine;
    bool boolEngine;
    int boolEngineLocation;
    int engineLocation;
    int CountOfPointsEngine;

    //Position of main window
    int MainwindowX;
    int MainwindowY;

    //normal vector
    float normalX;
    float normalY;

    QOpenGLFunctions opengl_functions;
    QOpenGLExtraFunctions other_opengl_functions;
    int scale;
    int move_x;
    int move_y;
    float step_grid_x;
    float step_grid_y;
    int CountOfLinesGrid_x;
    int CountOfLinesGrid_y;

    QOpenGLShaderProgram shader_program;
    QMatrix4x4 scale_move_matrix;

    void resetview();
    void PutPointsToNodes();

    void initShaders();
    void draw();
    void drawGrid();
    void drawEngine();

    void Circle();
    void paintLines();

    void CreateArrayForAnimation();
    int frames;

    float min(float, float);
    float max(float, float);

    virtual void mousePressEvent(QMouseEvent*) override;
    virtual void mouseReleaseEvent(QMouseEvent*) override;
    bool PointIsChecked;
    bool tmpPointIsChecked;
    int position;
    int tmpPosition;
    int NumberOfFrame;

public:
    graphic_widget();
    ~graphic_widget();
    void resizeGL(int w, int h) override;
    void initializeGL() override;
    void paintGL() override;
    void setSizeOfPoints(int);

    void getMainWindowX(int);
    void getMainWindowY(int);



public slots:
    void changedSize(QString);
    void changedCount(QString);
    void changedStepX(QString);
    void changedStepY(QString);
    void changedBoolGrid(bool);
    void graphic_getX(int);
    void graphic_getY(int);
    void Animation();
};

#endif // GRAPHIC_WIDGET_H
