#include "graphic_widget.h"
#include <cmath>
#include <iostream>

graphic_widget::graphic_widget()
{
    this->CountOfPoints = 12;
    this->SizeOfPoint = 5.0;
    this->CoordsOfPoints=new float[2*this->CountOfPoints];
    this->scale_move_matrix.setToIdentity();
    this->scale = 1;
    this->setFixedSize(600,600);
    move_x = 0;
    move_y = 0;
    step_grid_x = 0.02;
    step_grid_y = 0.02;
    CountOfLinesGrid_x = 2.0/step_grid_x + 1;
    CountOfLinesGrid_y = 2.0/step_grid_y + 1;
    grid_lines_x = new float[4*CountOfLinesGrid_x];
    grid_lines_y = new float[4*CountOfLinesGrid_y];
    boolGrid = true;
    PointIsChecked = false;
    tmpPointIsChecked = false;
    position = 0;
    tmpPosition = 0;
    normalX = 0;
    normalY = 0;
    frames = 50;
    this->CountOfPointsEngine = 1/step_grid_x;
    forEngine = new float[2*CountOfPointsEngine];
    boolEngine = false;
    NumberOfFrame = 0;
    this->setMouseTracking(true);
    this->Circle();
    this->PutPointsToNodes();
}

graphic_widget::~graphic_widget()
{
    delete[]this->CoordsOfPoints;
}

void graphic_widget::initShaders()
{
    this->opengl_functions.initializeOpenGLFunctions();
    this->other_opengl_functions.initializeOpenGLFunctions();
    shader_program.addShaderFromSourceFile (QOpenGLShader::Vertex, "D:/painter3/shader.vsh");
    shader_program.addShaderFromSourceFile(QOpenGLShader::Fragment, "D:/painter3/shader.fsh");
    if(shader_program.link()==false)
    {
        qDebug() << shader_program.log();
    }
    VertexLocation = shader_program.attributeLocation("vertex");

    ScaleMovingLocation = shader_program.uniformLocation("scale_move_matrix");
    SOPLocation = shader_program.attributeLocation("size_of_Point");
    vorlLocation = shader_program.uniformLocation("vorl");
    lineLocation = shader_program.attributeLocation("lines");
    this->boolEngineLocation = shader_program.uniformLocation("bool_engine");
    this->engineLocation = shader_program.attributeLocation("engine");
}

void graphic_widget::resizeGL(int width, int height)
{
    this->opengl_functions.glViewport(0,0,width, height);

}



void graphic_widget::initializeGL()
{

    this->qglClearColor(Qt::white);

    resetview();
    initShaders();

    this->other_opengl_functions.glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
}

void graphic_widget::paintGL()
{
    this->other_opengl_functions.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    this->resetview();
    if(boolGrid)
        this->drawGrid();
    this->drawEngine();
    this->draw();

}


void graphic_widget::resetview()
{
    this->qglClearColor(Qt::white);
    this->scale_move_matrix.setToIdentity();
    this->scale_move_matrix.translate(this->move_x, this->move_y);
    this->scale_move_matrix.scale(this->scale);
}

void graphic_widget::draw()
{
    if(this->CoordsOfPoints != nullptr)
    {
        shader_program.bind();
        shader_program.setAttributeArray(VertexLocation, CoordsOfPoints, 2);
        shader_program.setAttributeValue(SOPLocation, this->SizeOfPoint);
        shader_program.setUniformValue(ScaleMovingLocation, scale_move_matrix);
        shader_program.setUniformValue(vorlLocation, 1);
        shader_program.setUniformValue(boolEngineLocation, 0);
        shader_program.enableAttributeArray(VertexLocation);
        opengl_functions.glDrawArrays(GL_POINTS, 0, this->CountOfPoints);
        if(!NumberOfFrame)
        {
            opengl_functions.glDrawArrays(GL_LINE_LOOP, 0, this->CountOfPoints);
        }
        shader_program.disableAttributeArray(VertexLocation);
        shader_program.release();
        //qDebug() << this->x() << " " << QCursor::pos();
    }
}

void graphic_widget::drawGrid()
{
    for(int i = 0; i < CountOfLinesGrid_x;i++)
    {
        grid_lines_x[4*i] = -1.0+i*step_grid_x;
        grid_lines_x[4*i+1] = -1.0;
        grid_lines_x[4*i+2] = -1.0+i*step_grid_x;
        grid_lines_x[4*i+3] = 1.0;
    }
    for(int i = 0; i < CountOfLinesGrid_y;i++)
    {
        grid_lines_y[4*i] =  -1.0;
        grid_lines_y[4*i+1] = -1.0+i*step_grid_y;
        grid_lines_y[4*i+2] = 1.0;
        grid_lines_y[4*i+3] = -1.0+i*step_grid_y;
    }
    shader_program.bind();

    shader_program.setAttributeArray(lineLocation, grid_lines_x, 2);
    shader_program.setUniformValue(vorlLocation, 0);
    shader_program.setUniformValue(boolEngineLocation, 0);
    shader_program.enableAttributeArray(lineLocation);
    opengl_functions.glDrawArrays(GL_LINES, 0, 2*CountOfLinesGrid_x);
    shader_program.disableAttributeArray(lineLocation);

    shader_program.setAttributeArray(lineLocation, grid_lines_y, 2);
    shader_program.setUniformValue(vorlLocation, 0);
    shader_program.setUniformValue(boolEngineLocation, 0);
    shader_program.enableAttributeArray(lineLocation);
    opengl_functions.glDrawArrays(GL_LINES, 0, 2*CountOfLinesGrid_y);
    shader_program.disableAttributeArray(lineLocation);

    shader_program.release();
}

void graphic_widget::drawEngine()
{
    if(this->forEngine != nullptr)
    {
        for(int i =0; i < this->CountOfPointsEngine; i++)
        {
            this->forEngine[2*i] = cos(qDegreesToRadians(360.0*i/CountOfPointsEngine))*0.9;
            this->forEngine[2*i+1] = sin(qDegreesToRadians(360.0*i/CountOfPointsEngine))*0.9;
        }
        shader_program.bind();
        shader_program.setUniformValue(vorlLocation, 0);
        shader_program.setUniformValue(boolEngineLocation, 1);
        shader_program.setAttributeArray(engineLocation, forEngine, 2);
        shader_program.enableAttributeArray(engineLocation);
        opengl_functions.glDrawArrays(GL_LINE_LOOP, 0, CountOfPointsEngine);
        shader_program.disableAttributeArray(engineLocation);
        shader_program.release();
    }
}

void graphic_widget::Circle()
{
    if(this->CoordsOfPoints!=nullptr)
    {
        for(int i =0; i < this->CountOfPoints; i++)
        {
            //this->CoordsOfPoints[2*i] = 0.9*cos(qDegreesToRadians(360.0*i/CountOfPoints))*cos(qDegreesToRadians(360.0*i/CountOfPoints))*cos(qDegreesToRadians(360.0*i/CountOfPoints));
            //this->CoordsOfPoints[2*i+1] = 0.9*sin(qDegreesToRadians(360.0*i/CountOfPoints))*sin(qDegreesToRadians(360.0*i/CountOfPoints))*sin(qDegreesToRadians(360.0*i/CountOfPoints));
            this->CoordsOfPoints[2*i] = 0.7*cos(qDegreesToRadians(360.0*i/CountOfPoints));
            this->CoordsOfPoints[2*i+1] = 0.7*sin(qDegreesToRadians(360.0*i/CountOfPoints));
        }
    }
}

void graphic_widget::setSizeOfPoints(int size)
{
    this->SizeOfPoint = size;
}


void graphic_widget::changedSize(QString str)
{
    this->SizeOfPoint = str.toFloat();
    this->paintGL();
    this->swapBuffers();
}
void graphic_widget::changedCount(QString str)
{
    this->PointIsChecked = false;
    delete[]CoordsOfPoints;
    this->CountOfPoints = str.toInt();
    this->CoordsOfPoints = new float[2*CountOfPoints];
    this->Circle();
    this->PutPointsToNodes();
    this->paintGL();
    this->swapBuffers();
}

void graphic_widget::changedStepX(QString str)
{
    this->PointIsChecked =false;
    for(int i = 0; i < step_grid_x; i++)
    {
        delete []ForAnimation[i];
    }
    delete []ForAnimation;
    this->step_grid_x = str.toFloat();
    delete[]grid_lines_x;
    if(step_grid_x > 0)
    {
         CountOfLinesGrid_x = 2.0/step_grid_x + 1;
    }
    else
        CountOfLinesGrid_x = 0;
    this->grid_lines_x = new float[4*CountOfLinesGrid_x];
    this->CountOfPointsEngine = 2/step_grid_x;
    delete[]forEngine;
    forEngine = new float[2*CountOfPointsEngine];
    ForAnimation = new float*[CountOfLinesGrid_x+1];
    for(int i = 0; i < CountOfLinesGrid_x+1; i++)
    {
        ForAnimation[i] = new float[CountOfLinesGrid_y];
    }
    this->paintGL();
    this->swapBuffers();
}

void graphic_widget::changedStepY(QString str)
{
    this->PointIsChecked = false;
    for(int i = 0; i < step_grid_x; i++)
    {
        delete []ForAnimation[i];
    }
    this->step_grid_y = str.toFloat();
    delete[]grid_lines_y;
    if(step_grid_y > 0)
    {
         CountOfLinesGrid_y = 2.0/step_grid_y + 1;
    }
    else
    {
        CountOfLinesGrid_y = 0;
    }
    this->grid_lines_y = new float[4*CountOfLinesGrid_y];
    ForAnimation = new float*[CountOfLinesGrid_x+1];
    for(int i = 0; i < CountOfLinesGrid_x+1; i++)
    {
        ForAnimation[i] = new float[CountOfLinesGrid_y];
    }
    this->paintGL();
    this->swapBuffers();
}

void graphic_widget::changedBoolGrid(bool checked)
{
    this->boolGrid = checked;
    this->paintGL();
    this->swapBuffers();
}

void graphic_widget::PutPointsToNodes()
{
    if((this->step_grid_x>0)&&(step_grid_y >0))
    {
        for(int i = 0; i < CountOfPoints; i++)
        {
            float j = 0;
            while(!((CoordsOfPoints[2*i]>(j*step_grid_x - 1))&&(CoordsOfPoints[2*i]<=(step_grid_x*(j+1) - 1))))
            {
                j = j + 1;
            }
            float k = 0;
            while(!((CoordsOfPoints[2*i+1]>(k*step_grid_y - 1))&&(CoordsOfPoints[2*i+1]<=(step_grid_y*(k+1) - 1))))
            {
                k = k + 1;
            }

            float tmp = step_grid_x*(j+1) - 1 - CoordsOfPoints[2*i];
            if(tmp > step_grid_x/2)
            {
                CoordsOfPoints[2*i] = step_grid_x*j - 1;
            }
            else CoordsOfPoints[2*i] = step_grid_x*(j+1) - 1;

            tmp = step_grid_y*(k+1) - 1 - CoordsOfPoints[2*i+1];
            if(tmp > step_grid_y/2)
            {
                CoordsOfPoints[2*i+1] = step_grid_y*k - 1;
            }
            else CoordsOfPoints[2*i+1] = step_grid_y*(k+1) - 1;
        }
    }

}

void graphic_widget::getMainWindowX(int x)
{
    this->MainwindowX = x;
}

void graphic_widget::getMainWindowY(int y)
{
    this->MainwindowY = y;
}

void graphic_widget::graphic_getX(int x)
{
    this->MainwindowX = x + 11;
}

void graphic_widget::graphic_getY(int y)
{
    this->MainwindowY = y + 11;
}

void graphic_widget::mousePressEvent(QMouseEvent* event)
{
    float tmp_x = ((event->x()) - 300.)/300.0;
    float tmp_y = (300.0 - event->y())/300.0;
    while((tmpPosition<CountOfPoints)&&(!tmpPointIsChecked))
    {
        if((CoordsOfPoints[2*tmpPosition]>tmp_x-0.05)&&(CoordsOfPoints[2*tmpPosition]<tmp_x+0.05))
        {
            if((CoordsOfPoints[2*tmpPosition+1]>tmp_y-0.05)&&(CoordsOfPoints[2*tmpPosition+1]<tmp_y+0.05))
                tmpPointIsChecked = true;
            else tmpPosition++;
        }
        else
        {
            tmpPosition++;
        }

    }
    if(tmpPointIsChecked)
    {
        PointIsChecked = true;
        position = tmpPosition;
    }
    //qDebug() << tmp_x << tmp_y << position;
    tmpPosition = 0;
    tmpPointIsChecked = false;
}
void graphic_widget::mouseReleaseEvent(QMouseEvent* event)
{
    this->setMouseTracking(true);
    //qDebug() << ((event->x()) - 300.)/300.0 <<(300.0 - event->y())/300.0;
    if(PointIsChecked)
    {
        this->CoordsOfPoints[2*position] = ((event->x()) - 300.)/300.0;
        this->CoordsOfPoints[2*position+1] = (300.0 - event->y())/300.0;
        this->paintGL();
        this->swapBuffers();
    }
    PointIsChecked = false;
}

void graphic_widget::CreateArrayForAnimation()
{
    ForAnimation = new float*[CountOfLinesGrid_x+1];
    for(int j = 0; j <= CountOfLinesGrid_x;j++)
    {
        ForAnimation[j] = new float[CountOfLinesGrid_y+1];
    }

    /*float xMin = 1;
    float xMax = -1;
    float yMin = 1;
    float yMax = -1;
    for(int i = 0; i < CountOfPoints; i++)
    {
        if(CoordsOfPoints[2*i] > xMax)
        {
            xMax = CoordsOfPoints[2*i];
        }
        if(CoordsOfPoints[2*i] < xMin)
        {
            xMin = CoordsOfPoints[2*i];
        }
        if(CoordsOfPoints[2*i+1] > yMax)
        {
            yMax = CoordsOfPoints[2*i+1];
        }
        if(CoordsOfPoints[2*i+1] < yMin)
        {
            yMin = CoordsOfPoints[2*i+1];
        }
    }

    int iMin = (xMin+1)/step_grid_x;
    int iMax = (xMax+1)/step_grid_x;
    int jMin = (yMin+1)/step_grid_y;
    int jMax = (yMax+1)/step_grid_y;
    for(int i = 0; i < iMin; i++)
    {
        for(int j = 0; j <= CountOfLinesGrid_y; j++)
        {
            ForAnimation[i][j] = -1;
        }
    }
    for(int i = iMax+1; i <= CountOfLinesGrid_x; i++)
    {
        for(int j = 0; j <= CountOfLinesGrid_y; j++)
        {
            ForAnimation[i][j] = -1;
        }
    }
    for(int j = 0; j < jMin; j++)
    {
        for(int i = iMin; i <= iMax; i++)
        {
            ForAnimation[i][j] = -1;
        }
    }
    for(int j = jMax+1; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = iMin; i <= iMax; i++)
        {
            ForAnimation[i][j] = -1;
        }
    }*/

    float S1 = 0;
    float XiYi1 = 0;
    float Xi1Yi = 0;
    for(int i = 0; i < CountOfPoints-1; i++)
    {
        XiYi1 = XiYi1 + CoordsOfPoints[2*i]*CoordsOfPoints[2*(i+1)+1];
        Xi1Yi = Xi1Yi + CoordsOfPoints[2*(i+1)]*CoordsOfPoints[2*i+1];
    }
    S1 = abs(XiYi1 + CoordsOfPoints[2*(CountOfPoints-1)]*CoordsOfPoints[1] - Xi1Yi - CoordsOfPoints[0]*CoordsOfPoints[2*CountOfPoints - 1])/2;
    //qDebug() << S1*1;
    for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = 0; i <= CountOfLinesGrid_x;i++)
        {
            float S2 = 0;
            float x1 = step_grid_x*i-1;
            float y1 = step_grid_y*j-1;
            for(int k = 0; k < CountOfPoints - 1; k++)
            {
                float x2 = CoordsOfPoints[2*k];
                float y2 = CoordsOfPoints[2*k+1];
                float x3 = CoordsOfPoints[2*k+2];
                float y3 = CoordsOfPoints[2*k+3];
                S2 = S2 + abs((x1*(y2-y3) + x2*(y3 - y1) + x3*(y1-y2))/2);
            }
            float x21 = CoordsOfPoints[2*CountOfPoints-2];
            float y21 = CoordsOfPoints[2*CountOfPoints-1];
            float x31 = CoordsOfPoints[0];
            float y31 = CoordsOfPoints[1];
            S2 = S2 + abs((x1*(y21-y31) + x21*(y31 - y1) + x31*(y1-y21))/2);
            //S2 = abs(S2);
            if((abs(S2-S1) > 0.0001))
            {
                ForAnimation[i][j] = 1;
            }
            else
            {
                ForAnimation[i][j] = -1;
            }

        }
    }

    for(int i = 0; i < CountOfPoints-1; i++)
    {
        {
        int tmpX = (CoordsOfPoints[2*i]+1.0)/step_grid_x;
        if(abs(tmpX*step_grid_x - (CoordsOfPoints[2*i]+1))>step_grid_x/2)
        {
            tmpX++;
        }
        int tmpY = (CoordsOfPoints[2*i+1]+1.0)/step_grid_y;
        if(abs(tmpY*step_grid_y-(CoordsOfPoints[2*i+1]+1))>step_grid_y/2)
        {
            tmpY++;
        }
        int tmpX1 = (CoordsOfPoints[2*(i+1)]+1.0)/step_grid_x;
        if(abs(tmpX1*step_grid_x - (CoordsOfPoints[2*(i+1)]+1))>step_grid_x/2)
        {
            tmpX1++;
        }
        if(tmpY==34)
        {
            int e = 0;
        }
        int tmpY1 = (CoordsOfPoints[2*(i+1)+1]+1.0)/step_grid_y;
        if(abs(tmpY1*step_grid_y-(CoordsOfPoints[2*(i+1)+1]+1))>step_grid_y/2)
        {
            tmpY1++;
        }
        float xMin = tmpX;
        float xMax = tmpX1;
        if(tmpX1 < xMin)
        {
            xMin = tmpX1;
            xMax = tmpX;
        }
        float yMin = tmpY;
        float yMax = tmpY1;
        if(tmpY1 < yMin)
        {
            yMin = tmpY1;
            yMax = tmpY;
        }
        if(tmpY==tmpY1)
        {
            for(int j = xMin; j <= xMax; j++)
            {
                ForAnimation[j][tmpY] = 0;
            }
        }
        else if(tmpX==tmpX1)
        {
            for(int j = yMin; j <= yMax; j++)
            {
                ForAnimation[tmpX][j] = 0;
            }
        }
        else
        {

            for(int k = yMin; k <= yMax; k++)
            {

                for(int j = xMin; j <= xMax; j++)
                {
                    float x12 = tmpX*step_grid_x - 1;
                    float y12 = tmpY*step_grid_y - 1;
                    float x22 = tmpX1*step_grid_x - 1;
                    float y22 = tmpY1*step_grid_y - 1;
                    float a = (y22-y12)/(x22-x12);
                    float b = y12 - x12*(y22-y12)/(x22-x12);
                    if(abs((k*step_grid_y-1) - a*(j*step_grid_x-1) - b) < 0.001)
                    {
                        ForAnimation[j][k] = 0;
                    }
                }
            }
        }
    }
    }
    std::cout << "ForAnimation" << std::endl;
    for(int i = 0; i <=CountOfLinesGrid_x; i++)
    {
        for(int j = 0; j < CountOfLinesGrid_y; j++)
        {
            std::cout << ForAnimation[i][j] << " ";
        }
        std::cout << std::endl;
    }
}


/*void graphic_widget::CreateArrayForAnimation()
{
    ForAnimation = new float*[CountOfLinesGrid_x+1];
    for(int j = 0; j <= CountOfLinesGrid_x;j++)
    {
        ForAnimation[j] = new float[CountOfLinesGrid_y+1];
    }

    float xMin = 1;
    float xMax = -1;
    float yMin = 1;
    float yMax = -1;
    //Find min and max cooordinates of figure
    for(int i = 0; i < CountOfPoints; i++)
    {
        if(CoordsOfPoints[2*i] > xMax)
        {
            xMax = CoordsOfPoints[2*i];
        }
        if(CoordsOfPoints[2*i] < xMin)
        {
            xMin = CoordsOfPoints[2*i];
        }
        if(CoordsOfPoints[2*i+1] > yMax)
        {
            yMax = CoordsOfPoints[2*i+1];
        }
        if(CoordsOfPoints[2*i+1] < yMin)
        {
            yMin = CoordsOfPoints[2*i+1];
        }
    }


    int iMin = (xMin+1)/step_grid_x;
    int iMax = (xMax+1)/step_grid_x;
    int jMin = (yMin+1)/step_grid_y;
    int jMax = (yMax+1)/step_grid_y;

    for(int i = 0; i < CountOfLinesGrid_x+1; i++)
    {
        for(int j = 0; j < CountOfLinesGrid_y + 1; j++)
        {
            ForAnimation[i][j] = -1;
        }
    }

    for(int i = iMin; i<=iMax; i++)
    {
        for(int j = jMin; j <= jMax; j++)
        {
            int CountCrossing = 0;
            float gridX = i*step_grid_x - 1.0;
            float gridY = j*step_grid_y - 1.0;
            for(int k = 0; k < CountOfPoints - 1; k++)
            {
                if((i==6)&&(j==34))
                {
                    bool q = 1;
                }
                float X11 = CoordsOfPoints[2*k];
                float Y11 = CoordsOfPoints[2*k+1];
                float X21 = CoordsOfPoints[2*k+2];
                float Y21 = CoordsOfPoints[2*k+3];
                float a = (Y21-Y11)/(X21-X11);
                float b = Y11 - X11*(Y21-Y11)/(X21-X11);
                float x0 = (gridY - b)/a;
                if(((Y11>=gridY)&&(Y21<gridY))||((Y11<=gridY)&&(Y21>gridY)))
                {
                    if(abs(x0-gridX)<0.001)
                    {
                        ForAnimation[i][j] = 0;
                    }
                    else if((x0>gridX)&&(((X11 >= x0)&&(X21 < x0))||((X11 <= x0)&&(X21 > x0))))
                    {
                        CountCrossing++;
                    }
                }
            }
            float X11 = CoordsOfPoints[2*CountOfPoints - 2];
            float Y11 = CoordsOfPoints[2*CountOfPoints - 1];
            float X21 = CoordsOfPoints[0];
            float Y21 = CoordsOfPoints[1];
            float a = (Y21-Y11)/(X21-X11);
            float b = Y11 - X11*(Y21-Y11)/(X21-X11);
            float x0 = (gridY - b)/a;
            if(((Y11>=gridY)&&(Y21<gridY))||((Y11<=gridY)&&(Y21>gridY)))
            {
                if(abs(x0-gridX)<0.001)
                {
                    ForAnimation[i][j] = 0;
                }
                else if((x0>gridX)&&(((X11 > x0)&&(X21 < x0))||((X11 < x0)&&(X21 > x0))))
                {
                    CountCrossing++;
                }
            }
            if(ForAnimation[i][j]!=0)
            {
                if(CountCrossing%2)
                {
                    ForAnimation[i][j] = 1;
                }
            }
        }
     }
    for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = 0; i <= CountOfLinesGrid_x; i++)
        {
            qDebug() << ForAnimation[i][j] << i << j;
        }
    }
}*/

float graphic_widget::min(float a, float b)
{
    if(a < b)
    {
        return a;
    }
    else
    {
        return b;
    }
}

float graphic_widget::max(float a, float b)
{
    if(a>b)
    {
        return a;
    }
    else
    {
        return b;
    }
}

void graphic_widget::buildVelocity()
{
    if(this->velocity!=nullptr)
    {
        velocity = new float*[CountOfLinesGrid_x+1];
        for(int i = 0; i <= CountOfLinesGrid_x; i++)
        {
            velocity[i] = new float[CountOfLinesGrid_y+1];
        }
    }

    for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        velocity[0][j] = 0;
    }
    /*for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = 1; i <= CountOfLinesGrid_x; i++)
        {
//            if(j == 6)
//            {
//                bool z = 0;
//            }
            if((ForAnimation[i][j] >= 0)&&(ForAnimation[i-1][j]<0))
            {
                velocity[i][j] = 0.5;
                //velocity[i+1][j] = 0.5;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i+1][j] < 0))
            {
                velocity[i][j] = 0.5;
                //velocity[i-1][j] = 0.5;
            }
            else
            {
                if(velocity[i][j]!=0.5)
                {
                    velocity[i][j] = 0;
                }
            }
        }
    }*/
    for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = 0; i <= CountOfLinesGrid_x; i++)
        {
            if(ForAnimation[i][j]>=0)
            {
                velocity[i][j] = 0.1;
            }
            else
            {
                velocity[i][j] = 0;
            }
        }
    }
    std::cout << "Velocity" << std::endl;
    for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = 0; i <= CountOfLinesGrid_x; i++)
        {
            std::cout << velocity[i][j] << " ";
        }
        std::cout << j << std::endl;
    }
}



void graphic_widget::createNew_LSF()
{
    New_LSF = new float*[CountOfLinesGrid_x+1];
    for(int i = 0; i <= CountOfLinesGrid_x; i++)
    {
        New_LSF[i] = new float[CountOfLinesGrid_y+1];
    }
    for(int i = 0; i <= CountOfLinesGrid_x; i++)
    {
        New_LSF[i][0] = 1;
        New_LSF[i][CountOfLinesGrid_y] = 1;
    }
    for(int j = 1; j <= CountOfLinesGrid_y; j++)
    {
        New_LSF[0][j] = 1;
        New_LSF[CountOfLinesGrid_y][j] = 1;
    }
    for(int i = 1; i < CountOfLinesGrid_x; i++)
    {
        for(int j = 1; j < CountOfLinesGrid_y; j++)
        {
            float Dij_x = (ForAnimation[i][j] - ForAnimation[i-1][j])/step_grid_x;
            float Dijx = (ForAnimation[i+1][j] - ForAnimation[i][j])/step_grid_x;
            float Dij_y = (ForAnimation[i][j] - ForAnimation[i][j-1])/step_grid_y;
            float Dijy = (ForAnimation[i][j+1] - ForAnimation[i][j])/step_grid_y;

            float numbla_plus_ = (max(Dij_x, 0))*(max(Dij_x, 0)) + (min(Dijx, 0))*(min(Dijx, 0))
                                + (max(Dij_y, 0))*(max(Dij_y, 0)) + (min(Dijy, 0))*(min(Dijy, 0));
            float numbla_plus = pow(numbla_plus_, 0.5);

            float numbla_minus_ = (min(Dij_x, 0))*(min(Dij_x, 0)) + (max(Dijx, 0))*(max(Dijx, 0))
                                + (min(Dij_y, 0))*(min(Dij_y, 0)) + (max(Dijy, 0))*(max(Dijy, 0));
            float numbla_minus = pow(numbla_minus_, 0.5);
            New_LSF[i][j] = ForAnimation[i][j] - (0.1)*(max(velocity[i][j], 0)*numbla_plus + min(velocity[i][j], 0)*numbla_minus);
        }
    }
    int k = 0;
    for(int j = 1; j < CountOfLinesGrid_y; j++)
    {
        for(int i = 1; i < CountOfLinesGrid_x; i++)
        {
            float x1 = i*step_grid_x-1;
            float y1 = j*step_grid_y - 1;
            if((ForAnimation[i][j] >= 0)&&(ForAnimation[i-1][j]<0)&&(x1*x1+y1*y1<0.8))
            {
                k++;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i+1][j] < 0)&&(x1*x1+y1*y1<0.8))
            {
                k++;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i][j-1]<0)&&(x1*x1+y1*y1<0.8))
            {
                k++;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i][j+1] < 0)&&(x1*x1+y1*y1<0.8))
            {
                k++;
            }
        }
    }
    float* New_coordinates = new float[2*k];
    int q = 0;
    for(int j = 1; j < CountOfLinesGrid_y; j++)
    {
        for(int i = 1; i < CountOfLinesGrid_x; i++)
        {
            float x1 = i*step_grid_x-1;
            float y1 = j*step_grid_y - 1;

            if((ForAnimation[i][j] >= 0)&&(ForAnimation[i-1][j]<0)&&(x1*x1+y1*y1<0.8))
            {
                New_coordinates[2*q] = i*step_grid_x - 1;
                New_coordinates[2*q+1] = j*step_grid_y - 1;
                q++;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i+1][j] < 0)&&(x1*x1+y1*y1<0.8))
            {
                New_coordinates[2*q] = i*step_grid_x - 1;
                New_coordinates[2*q+1] = j*step_grid_y - 1;
                q++;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i][j-1]<0)&&(x1*x1+y1*y1<0.8))
            {
                New_coordinates[2*q] = i*step_grid_x - 1;
                New_coordinates[2*q+1] = j*step_grid_y - 1;
                q++;
            }
            else if((ForAnimation[i][j] >= 0)&&(ForAnimation[i][j+1] < 0)&&(x1*x1+y1*y1<0.8))
            {
                New_coordinates[2*q] = i*step_grid_x - 1;
                New_coordinates[2*q+1] = j*step_grid_y - 1;
                q++;
            }
        }
    }
    delete[]CoordsOfPoints;
    CountOfPoints = k;
    CoordsOfPoints = new float[2*k];
    for(int i = 0; i < CountOfPoints; i++)
    {
        CoordsOfPoints[2*i] = New_coordinates[2*i];
        CoordsOfPoints[2*i+1] = New_coordinates[2*i+1];
    }
    for(int i = 0; i <= CountOfLinesGrid_x; i++)
    {
        for(int j = 0; j <= CountOfLinesGrid_y; j++)
        {
            ForAnimation[i][j] = New_LSF[i][j];
        }
    }
    std::cout << "New_LSF" << std::endl;
    for(int j = 0; j <= CountOfLinesGrid_y; j++)
    {
        for(int i = 0; i <= CountOfLinesGrid_x; i++)
        {
            std::cout << New_LSF[i][j]  << " ";
        }
        std::cout << j << std::endl;
    }
    for(int i = 0; i < CountOfLinesGrid_x; i++)
    {
        delete[]New_LSF[i];
    }
    delete[]New_LSF;
}

void graphic_widget::Animation()
{

    if(!NumberOfFrame)
    {
        this->CreateArrayForAnimation();
    }
    NumberOfFrame++;
    this->buildVelocity();
    this->createNew_LSF();
    this->paintGL();
    this->swapBuffers();

}
