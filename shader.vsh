attribute vec2 vertex;
attribute vec4 lines;
uniform mat4 scale_move_matrix;
attribute float size_of_Point;
uniform bool vorl;
varying vec4 color;
attribute vec2 engine;
uniform bool bool_engine;



void main(void)
{
    if(vorl)
        gl_Position = vec4(vertex, 0.0, 1.0)*scale_move_matrix;
    else if(bool_engine)
        gl_Position = vec4(engine, 0.0, 1.0)*scale_move_matrix;
    else gl_Position = lines;
    if(vorl)
        color = vec4(1.0, 0.0, 0.0, 1.0);
    else if (bool_engine)
        color = vec4(0.0, 0.0, 0.0, 1.0);
    else  color = vec4(0.8, 0.8, 0.8, 1.0);
    gl_PointSize = size_of_Point;
}
