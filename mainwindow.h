#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "central_widget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setX(int);
    void setY(int);

private:
    //central_widget wid;
    Ui::MainWindow *ui;
    int WindowX;
    int WindowY;
public:
signals:
    void sendX(int);
    void sendY(int);
};
#endif // MAINWINDOW_H
