#include <QSizePolicy>
#include "central_widget.h"

central_widget::central_widget()
{
    glwid.getMainWindowX(MainX);
    glwid.getMainWindowY(MainY);
    layout.addWidget(&glwid,0,0,20,20);
    scbar.setOrientation(Qt::Horizontal);
    layout.addWidget(&scbar,21,0,1,20);
    COPoints.setText("12");
    COPoints.setFixedWidth(60);
    lbl_COPoints.setText("Count of Points");
    SOPoints.setText("5");
    SOPoints.setFixedWidth(60);
    lbl_SOPoints.setText("Size of point");

    layout.addWidget(&COPoints,0,21,1,1);
    layout.addWidget(&lbl_COPoints,0,22,1,1);

    layout.addWidget(&SOPoints,1,21,1,1);
    layout.addWidget(&lbl_SOPoints, 1, 22, 1,1);

    COFrames.setText("50");
    COFrames.setFixedWidth(60);
    lbl_COFrames.setText("Count of frames for animation");
    layout.addWidget(&COFrames, 2,21,1,1);
    layout.addWidget(&lbl_COFrames,2,22,1,1);

    stepX.setText("0.05");
    stepX.setFixedWidth(60);
    label_stepX.setText("Step of grid by X");
    layout.addWidget(&stepX, 3, 21, 1, 1);
    layout.addWidget(&label_stepX, 3, 22, 1, 1);

    stepY.setText("0.05");
    stepY.setFixedWidth(60);
    label_stepY.setText("Step of grid by Y");
    layout.addWidget(&stepY, 4, 21, 1, 1);
    layout.addWidget(&label_stepY, 4, 22, 1, 1);

    button_grid.setText("Draw grid");
    button_grid.setChecked(true);
    layout.addWidget(&button_grid, 5,21);

    Animation.setText("Start animation");
    layout.addWidget(&Animation, 21,21);

    QObject::connect(&COPoints, SIGNAL(textChanged(QString)), &glwid, SLOT(changedCount(QString)));
    QObject::connect(&SOPoints, SIGNAL(textChanged(QString)), &glwid, SLOT(changedSize(QString)));
    QObject::connect(&stepX, SIGNAL(textChanged(QString)), &glwid, SLOT(changedStepX(QString)));
    QObject::connect(&stepY, SIGNAL(textChanged(QString)), &glwid, SLOT(changedStepY(QString)));
    QObject::connect(&button_grid, SIGNAL(toggled(bool)), &glwid, SLOT(changedBoolGrid(bool)));
    QObject::connect(this, SIGNAL(central_sendX(int)), &glwid, SLOT(graphic_getX(int)));
    QObject::connect(this, SIGNAL(central_sendY(int)), &glwid, SLOT(graphic_getY(int)));
    QObject::connect(&Animation, SIGNAL(clicked()), &glwid, SLOT(Animation()));


    this->setLayout(&layout);
}

void central_widget::getMainWindowX(int x)
{
    this->MainX = x;
}

void central_widget::getMainWindowY(int y)
{
    this->MainY = y;
}

void central_widget::getX(int x)
{
    emit central_sendX(x);
}

void central_widget::getY(int y)
{
    emit central_sendY(y);
}
