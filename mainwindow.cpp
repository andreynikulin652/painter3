#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    central_widget* wid = new central_widget();
    this->setCentralWidget(wid);
    QObject::connect(this, SIGNAL(sendX(int)), wid, SLOT(getX(int)));
    QObject::connect(this, SIGNAL(sendY(int)), wid, SLOT(getY(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setX(int x)
{
    this->WindowX = x;
}

void MainWindow::setY(int y)
{
    this->WindowY  =y ;
}
