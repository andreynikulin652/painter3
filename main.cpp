#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    //w.showMaximized();
    w.show();
    emit w.sendX(w.geometry().x());
    emit w.sendY(w.geometry().y());
    return a.exec();
}
